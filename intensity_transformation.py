#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  1 09:37:23 2018

@author: cristopher
"""

import cv2
import numpy as np

def nothing(x):
    pass

cv2.namedWindow("img", cv2.WINDOW_KEEPRATIO)
cv2.namedWindow("img2", cv2.WINDOW_KEEPRATIO)

img = cv2.imread("images/cat.jpg", cv2.IMREAD_COLOR)

ksizex = 2
ksizey = 3

cv2.createTrackbar("ksizex", "img2", 0, 63, nothing)
cv2.createTrackbar("ksizey", "img2", 0, 63, nothing)

while(True):    
    k = cv2.waitKey(1)
    if k == 27:
        break
    
    ksizex = cv2.getTrackbarPos('ksizex','img')
    ksizey = cv2.getTrackbarPos('ksizey','img')
    
    if(ksizex < 1):
        ksizex = 1
    
    if(ksizey < 1):
        ksizey = 1
        
    img2 = cv2.blur(img, (ksizex, ksizey), cv2.BORDER_DEFAULT)
            
    cv2.imshow("img", img)
    cv2.imshow("img2", img2)
    
cv2.destroyAllWindows()