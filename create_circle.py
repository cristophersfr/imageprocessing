#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 13 09:50:25 2018

@author: cristopher
"""

import cv2
import numpy as np

def nothing(x):
    pass

cv2.namedWindow('img', cv2.WINDOW_KEEPRATIO)

img = np.zeros((200,200), np.uint8)

xc = 100
yc = 100
radius = 50

cv2.createTrackbar('xc', 'img', 0, 200, nothing)
cv2.createTrackbar('yc', 'img', 0, 200, nothing)
cv2.createTrackbar('radius', 'img', 0, 200, nothing)

cv2.circle(img, (xc, yc), radius, 255, -1)

while(True):    
    
    xc = cv2.getTrackbarPos('xc','img')
    yc = cv2.getTrackbarPos('yc','img')
    radius = cv2.getTrackbarPos('radius','img')
    
    img = np.zeros((200,200), np.uint8)
    cv2.circle(img, (xc, yc), radius, 255, -1)
    
    cv2.imshow("img", img)
    
    k = cv2.waitKey(1)
    if k == 27:
        break
    
cv2.destroyAllWindows()
