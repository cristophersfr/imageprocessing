#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 12 10:04:59 2018

@author: cristopher
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt

img1 = cv2.imread('images/text.jpg')
img2 = cv2.imread('images/lena.jpg')
width, height, c = img2.shape
img1 = cv2.resize(img1, (width, height))

img_and = cv2.bitwise_and(img1,img2)

img_or = cv2.bitwise_or(img1,img2)

img_not = cv2.bitwise_not(img1)

