#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  3 10:34:39 2018

@author: cristopher
"""

import cv2
import numpy as np


input_image = np.array((
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 255, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0]), dtype="uint8")

cv2.namedWindow('img', cv2.WINDOW_KEEPRATIO)

MORPH = cv2.MORPH_DILATE
KERNEL_SIZE = 3
KERNEL_TYPE = cv2.MORPH_CROSS

kernel = cv2.getStructuringElement(KERNEL_TYPE,(3,3))

while True: 
        
    cv2.imshow("img", input_image)
    
    k = cv2.waitKey(1)
    if k == 27:
        break
    
cv2.destroyAllWindows()