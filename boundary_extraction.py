#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  3 10:11:30 2018

@author: cristopher
"""

import cv2
import numpy as np

input_image = cv2.imread('images/lena.jpg')

kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(3,3))
op = cv2.MORPH_ERODE

cv2.namedWindow('img', cv2.WINDOW_KEEPRATIO)
cv2.namedWindow('result', cv2.WINDOW_KEEPRATIO)

result = cv2.morphologyEx(input_image, op, kernel)

beta = input_image - result

while True:
    cv2.imshow('img', beta)
    cv2.imshow('result', result)
    
    k = cv2.waitKey(500)
    if k == 27:
        break
    
cv2.destroyAllWindows()
