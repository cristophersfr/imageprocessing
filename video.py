import cv2
import numpy as np

vid = cv2.VideoCapture(0)

if vid.isOpened():
    print('Webcam OK')
else:
    print('ERROR: Webcam not found')

while(True):
    _, frame = vid.read()
    cv2.imshow('frame', frame)
    if cv2.waitKey(1) == ord('q'):
        break

cv2.destroyAllWindows()
vid.release()