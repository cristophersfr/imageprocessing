#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 16 22:55:32 2018

@author: cristopher
"""

import cv2
import numpy as np

cv2.namedWindow('img', cv2.WINDOW_KEEPRATIO)
cv2.namedWindow('mask', cv2.WINDOW_KEEPRATIO)
cv2.namedWindow('result', cv2.WINDOW_KEEPRATIO)

def nothing(x):
    pass

lh, ls, lv = [0,0,0]
uh, us, uv = [255,255,255]

cv2.createTrackbar("lh", "img", 0, 255, nothing)
cv2.createTrackbar("ls", "img", 0, 255, nothing)
cv2.createTrackbar("lv", "img", 0, 255, nothing)

cv2.createTrackbar("uh", "img", 0, 255, nothing)
cv2.createTrackbar("us", "img", 0, 255, nothing)
cv2.createTrackbar("uv", "img", 0, 255, nothing)

while(1):
    frame = cv2.imread('images/chips.png')
    
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    
    lh = cv2.getTrackbarPos('lh','img')
    ls = cv2.getTrackbarPos('ls','img')
    lv = cv2.getTrackbarPos('lv','img')
    
    uh = cv2.getTrackbarPos('uh','img')
    us = cv2.getTrackbarPos('us','img')
    uv = cv2.getTrackbarPos('uv','img')
    
    lower_limit = np.array([lh, ls, lv])
    upper_limit = np.array([uh, us, uv])
    
    mask = cv2.inRange(hsv, lower_limit, upper_limit)
    res = cv2.bitwise_and(frame, frame, mask = mask)

    cv2.imshow('img',frame)
    cv2.imshow('mask',mask)
    cv2.imshow('result',res)
    
    k = cv2.waitKey(500) & 0xFF
    if k == ord('s'):
        print("Lower limits:")
        print("H, S, V")
        print(lh, ls, lv)
        print("Upper limits:")
        print("H, S, V")
        print(uh, us, uv)
    elif k == 27:
        break

cv2.destroyAllWindows()
