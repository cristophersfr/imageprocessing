#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 12 10:26:46 2018

@author: cristopher
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('images/broken-text.tif',0)
kernel = np.array([[0,1,0],
                  [1,1,1],
                  [0,1,0]], dtype = np.uint8)

dilation = cv2.dilate(img,kernel,iterations = 1)

plt.imshow(dilation, cmap = 'gray')