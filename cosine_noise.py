#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 15 09:50:15 2018

@author: cristopher
"""

import cv2
import numpy as np

def nothing(x):
    pass

img = cv2.imread('cat.jpg', 0)

cv2.namedWindow('img', cv2.WINDOW_KEEPRATIO)
cv2.namedWindow('spectrum', cv2.WINDOW_KEEPRATIO)
cv2.namedWindow('cosine', cv2.WINDOW_KEEPRATIO)
cv2.namedWindow('mask', cv2.WINDOW_KEEPRATIO)
cv2.namedWindow('img2', cv2.WINDOW_KEEPRATIO)

cv2.createTrackbar('freq', 'cosine', 0, 500, nothing)
cv2.createTrackbar('theta', 'cosine', 0, 100, nothing)
cv2.createTrackbar('gain', 'cosine', 0, 1000, nothing)
cv2.createTrackbar('inner_radius', 'mask', 0, 600, nothing)
cv2.createTrackbar('outter_radius', 'mask', 0, 600, nothing)

def createCircleMask(rows, cols, inner_radius, outter_radius):
    mask = np.zeros((img.shape[0], img.shape[1], 2), np.uint8)
    mask.fill(255)
    cv2.circle(mask, (np.int(img.shape[1]/2), np.int(img.shape[0]/2)), outter_radius, (0,0), -1)
    cv2.circle(mask, (np.int(img.shape[1]/2), np.int(img.shape[0]/2)), inner_radius, (255,255), -1)
    return mask

def magnitude_spectrum(img):
    dft = cv2.dft(np.float32(img),flags = cv2.DFT_COMPLEX_OUTPUT)
    dft_shift = np.fft.fftshift(dft)
    magnitude_spectrum = 20*np.log(cv2.magnitude(dft_shift[:,:,0],dft_shift[:,:,1]))
    cv2.normalize(magnitude_spectrum, magnitude_spectrum, 1, 0, cv2.NORM_MINMAX)
    return magnitude_spectrum

def createCosineImgMeshGrid(rows, cols, freq, theta):
    xx, yy = np.meshgrid(range(0, cols), range(0, rows), sparse=False)
    rho = xx*np.cos(theta) - yy*np.sin(theta)
    cosine = np.cos(2*np.pi*freq*rho)
    return cosine

def createCosineImg(rows, cols, freq, theta):
    img = np.zeros((rows,cols), np.float32)
    
    for x in range(img.shape[0]-1):
        for y in range(img.shape[1]-1):
            rho = x*np.cos(theta) - y*np.sin(theta)
            img[x][y] = np.cos(2*np.pi*freq*rho)
            
    return img

freq = 0
theta = 0
gain = 0

while(True):
    
    k = cv2.waitKey(500)
    if k == 27:
        break
    
    # Create trackbars and get variables
    freq = cv2.getTrackbarPos('freq', 'cosine')
    freq = freq*0.001
    theta = cv2.getTrackbarPos('theta', 'cosine')
    theta = theta*0.01
    gain = cv2.getTrackbarPos('gain', 'cosine')
    gain = gain*0.1
    
    inner_radius = cv2.getTrackbarPos('inner_radius', 'mask')
    outter_radius = cv2.getTrackbarPos('outter_radius', 'mask')
    
    # Create a cosine image 
    cosine = createCosineImgMeshGrid(img.shape[0], img.shape[1], freq, theta)
    
    # Add the cosine image to the original image.
    noise = img + gain*cosine
    
    # Find the magnitude_spectrum of the noise
    spectrum = magnitude_spectrum(noise)
    
    # Create a ring mask to overlap the cosine noise
    mask = createCircleMask(img.shape[0], img.shape[1], 
                            inner_radius, outter_radius)
    
    dft = cv2.dft(np.float32(noise),flags = cv2.DFT_COMPLEX_OUTPUT)
    dft_shift = np.fft.fftshift(dft)
    fshift = dft_shift*mask
    f_ishift = np.fft.ifftshift(fshift)
    img_back = cv2.idft(f_ishift)
    img_back = cv2.magnitude(img_back[:,:,0],img_back[:,:,1])
    
    cv2.normalize(img_back, img_back, 1, 0, cv2.NORM_MINMAX)    
    cv2.normalize(noise, noise, 1, 0, cv2.NORM_MINMAX)
    cv2.normalize(cosine, cosine, 1, 0, cv2.NORM_MINMAX)
    
    cv2.imshow('cosine', cosine)
    cv2.imshow('spectrum', spectrum)
    cv2.imshow('img', noise)
    cv2.imshow('mask', mask[:,:,0])
    cv2.imshow('img2', img_back)
    
cv2.destroyAllWindows()