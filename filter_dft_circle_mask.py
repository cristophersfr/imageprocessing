#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 13 10:23:30 2018

@author: cristopher
"""

import cv2
import numpy as np

def nothing(x):
    pass

cv2.namedWindow('mask', cv2.WINDOW_KEEPRATIO)

img = cv2.imread("cat.jpg", 0)

dft = cv2.dft(np.float32(img),flags = cv2.DFT_COMPLEX_OUTPUT)
dft_shift = np.fft.fftshift(dft)

radius = 200

cv2.createTrackbar('radius', 'mask', 0, 600, nothing)

while True:
    radius = cv2.getTrackbarPos('radius','mask')
    mask = np.zeros((img.shape[0], img.shape[1], 2), np.uint8)
    cv2.circle(mask, (np.int(img.shape[1]/2), np.int(img.shape[0]/2)), radius, (255,255), -1)

    fshift = dft_shift*mask
    f_ishift = np.fft.ifftshift(fshift)
    img_back = cv2.idft(f_ishift)
    img_back = cv2.magnitude(img_back[:,:,0],img_back[:,:,1])
    
    cv2.normalize(img_back, img_back, 1, 0, cv2.NORM_MINMAX)
        
    cv2.imshow('img', img_back)
    cv2.imshow('mask', mask[:,:,0])
    
    k = cv2.waitKey(500)
    if k == 27:
        break