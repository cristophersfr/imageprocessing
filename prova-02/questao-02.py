#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Mini Prova Surpresa de PDI

Questão 02: Aplique uma filtragem à imagem "eye.png" para melhorar
 a nitidez dos detalhes da íris.

Created on Tue Mar 20 10:20:43 2018

@author: cristopher
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt

def nothing(x):
    pass

