#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Mini Prova Surpresa de PDI

Questão 03: A imagem "coded_message.png" contém uma mensagem cifrada numa das suas
camadas binárias. Escreva o algoritmo que decifra esse código.
Created on Tue Mar 20 10:24:20 2018

@author: cristopher
"""

im