#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Mini Prova Surpresa de PDI

Questão 01: Aplique uma transformação de intensidade à imagem "lady.png" 
para aprimorar seu contraste.

Created on Tue Mar 20 09:49:48 2018

@author: cristopher
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt

def nothing(x):
    pass

cv2.namedWindow('img', cv2.WINDOW_KEEPRATIO)
cv2.namedWindow('img2', cv2.WINDOW_KEEPRATIO)

img = cv2.imread("lady1.png", 0)

cv2.createTrackbar("gain", "img2", 0, 100, nothing)

gain = 0

while(True):    
    k = cv2.waitKey(500)
    if k == 27:
        break
    
    gain = cv2.getTrackbarPos("gain","img2")
    
    gain = gain*0.001
        
    img2 = gain*img
            
    cv2.imshow("img", img)
    cv2.imshow("img2", img2)
    
cv2.destroyAllWindows()