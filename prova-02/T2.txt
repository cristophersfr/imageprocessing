Q01 Aplique uma transformação de intensidade à imagem "lady.png" para aprimorar seu contraste.

Q02 Aplique uma filtragem à imagem "eye.png" para melhorar a nitidez dos detalhes da íris.

Q03 A imagem "coded_message.png" contém uma mensagem cifrada numa das suas camadas binárias. Escreva o algoritmo que decifra esse código.

Q04 Resolva o que se pede.
(a) Carregue a imagem "ckt.tif" e obtenha as imagens gx e gy, contendo os gradientes nas direções x e y, respectivamente, usando filtragem no domínio espacial.
(b) Obtenha uma terceira imagem 'g' que é a aproximação do gradiente dada por g = |gx| + |gy|. Escalone esta imagem para a faixa [0, 255].
(c) Obtenha a imagem binária 'b' que contenha o valor '1' nas coordenadas dos pixels de g que são maiores ou iguais a 60 e zero caso contrário.
(d) Em que tipo de aplicação essa abordagem poderia ser útil?

Q05 Filtre a imagem "lena_noise.png" para atenuar todos os ruídos periódicos presentes na imagem. Salve o resultado.
