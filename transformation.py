#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 10:04:57 2018

@author: cristopher
"""

import cv2
import numpy as np

def nothing(x):
    pass

img = cv2.imread("spectrum.tif", cv2.IMREAD_GRAYSCALE)

img2 = np.zeros(img.shape, dtype = np.float64)

n = 50

cv2.namedWindow("image", cv2.WINDOW_KEEPRATIO)
cv2.createTrackbar('n','image',0,1000,nothing)

while(1):
    cv2.imshow('image',img2)
    
    k = cv2.waitKey(500)
    if k == 27:
        break
    
    print("passou")

    # get current positions of four trackbars
    n = cv2.getTrackbarPos('n','image')
    
    for x in range(img.shape[0]):
        for y in range(img.shape[1]):
            intensity = np.float64(img[x][y])
            # exponential transformation
            intensity_new = np.power(intensity, 25*n/1000)
            # logarithmic transformation
            # intensity_new = c * np.log(1 + intensity)
            img2[x][y] = intensity_new
            
    cv2.normalize(img2, img2, 1, 0, cv2.NORM_MINMAX)

cv2.destroyAllWindows()