#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 17 10:30:12 2018

@author: cristopher
"""

import cv2
import numpy as np

MORPH = [cv2.MORPH_ERODE, cv2.MORPH_DILATE, cv2.MORPH_OPEN, cv2.MORPH_CLOSE]

KERNEL_SIZE = [1,3,5,7,9,11,13,15,17,19,21]

KERNEL_TYPE=[cv2.MORPH_RECT, cv2.MORPH_ELLIPSE, cv2.MORPH_CROSS]
          
def nothing(x):
    pass

cv2.namedWindow('img', cv2.WINDOW_KEEPRATIO)
cv2.namedWindow('result', cv2.WINDOW_KEEPRATIO)

cv2.createTrackbar('op', 'img', 0, 3, nothing)
cv2.createTrackbar('wsize', 'img', 0, len(KERNEL_SIZE)-1, nothing)
cv2.createTrackbar('type', 'img', 0, 2, nothing)

img = cv2.imread('images/noisy-fingerprint.tif',0)

while(True):
    
    op = MORPH[cv2.getTrackbarPos('op','img')]
    wsize = KERNEL_SIZE[cv2.getTrackbarPos('wsize','img')]
    kernel_type = KERNEL_TYPE[cv2.getTrackbarPos('type','img')]
    
    kernel = cv2.getStructuringElement(kernel_type,(wsize,wsize))
    
    result = cv2.morphologyEx(img, op, kernel)
    
    cv2.imshow('img', img)
    cv2.imshow('result', result)
    
    k = cv2.waitKey(500)
    if k == 27:
        break
    
cv2.destroyAllWindows()
        
        
    


