#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 23:37:18 2018

@author: cristopher
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt

def gray_hist(img):
    '''
    This function generates a histogram for a gray image.
    '''
    hist = cv2.calcHist([img],[0],None,[256],[0,256])
    plt.plot(hist, color = 'black')
    plt.xlim([0,256])

    
def color_hist(img):
    '''
    This function generates a histogram for a colorful image. 
    '''
    color = ('b','g','r')
    hist = []
    for i,col in enumerate(color):
        hist = cv2.calcHist([img],[i],None,[256],[0,256])
        plt.plot(hist,color = col)
        plt.xlim([0,256])
    
if __name__ == "__main__":
    
    plt.figure(1,(10, 8))
    
    # Step 1:
    img = cv2.imread('cat.jpg', cv2.IMREAD_GRAYSCALE)
    plt.subplot(421)
    plt.title('Gray Image')
    plt.imshow(img, cmap = 'gray')
    plt.xticks([]), plt.yticks([])  
    plt.subplot(422)
    gray_hist(img)

    
    # Step 2:
    img = cv2.imread('cat.jpg')
    b,g,r = cv2.split(img)       # get b,g,r
    rgb_img = cv2.merge([r,g,b])  
    plt.subplot(423)
    plt.title('RGB Image')
    plt.imshow(rgb_img)
    plt.xticks([]), plt.yticks([])  
    plt.subplot(424)
    color_hist(img)
    
    # Step 3:
    plt.subplot(425)
    plt.title('Randu Noise')
    noise = np.random.randint(0, 255, (1000,1000,3))
    noise = np.uint8(noise)
    plt.imshow(noise)
    plt.xticks([]), plt.yticks([])  
    plt.subplot(426)
    color_hist(noise)    

    # Step 4: 
    plt.subplot(427)
    plt.title('Randn Noise')
    im = np.zeros((1000,1000,3), np.uint8)    
    m = (0,0,0) 
    s = (255,255,255)
    cv2.randn(im,m,s)
    plt.imshow(im)
    plt.xticks([]), plt.yticks([])  
    plt.subplot(428)
    color_hist(im)

    plt.show()
    
