#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  8 00:51:49 2018

@author: cristopher
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread("noisy_img.png")

img = cv2.medianBlur(img, 3)

plt.imshow(img)
plt.show()
