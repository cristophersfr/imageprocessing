#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  1 18:04:21 2018

@author: cristopher
"""

import cv2
import numpy as np

def magnitude_spectrum(img):
    dft = cv2.dft(np.float32(img),flags = cv2.DFT_COMPLEX_OUTPUT)
    dft_shift = np.fft.fftshift(dft)
    magnitude_spectrum = 20*np.log(cv2.magnitude(dft_shift[:,:,0],dft_shift[:,:,1]))
    cv2.normalize(magnitude_spectrum, magnitude_spectrum, 1, 0, cv2.NORM_MINMAX)
    return magnitude_spectrum

cv2.namedWindow("img", cv2.WINDOW_KEEPRATIO)
cv2.namedWindow("spectrum", cv2.WINDOW_KEEPRATIO)

img = cv2.imread("cat.jpg", cv2.IMREAD_GRAYSCALE)

img2 = img
spectrum = magnitude_spectrum(img2)

print("Selecione o filtro desejado")
print("(a) Filtro da Média")
print("(s) Filtro da Gaussiana")
print("(d) Filtro da Mediana")
print("(f) Filtro Bilateral")
print("(b) Remover filtragem")

while(True):    
    k = cv2.waitKey(1)
    if k == 27: # ESC
        break
    
    if k == ord('a'):
        # Filtro da média
        img2 = cv2.blur(img, (5,5))
        spectrum = magnitude_spectrum(img2)
    
    if k == ord('s'):
        # Filtro da Gaussiana
        img2 = cv2.GaussianBlur(img,(5,5),0)
        spectrum = magnitude_spectrum(img2)
    
    if k == ord('d'):
        # Filtro da Mediana
        img2 = cv2.medianBlur(img,5)
        spectrum = magnitude_spectrum(img2)

    if k == ord('f'):    
        # Filtro Bilateral
        img2 = cv2.bilateralFilter(img,9,75,75)
        spectrum = magnitude_spectrum(img2)
        
    if k == ord('b'):
        img2 = img
        spectrum = magnitude_spectrum(img2)
    
    cv2.imshow("img", img)
    cv2.imshow("spectrum", spectrum)

cv2.destroyAllWindows()