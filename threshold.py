#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 10:57:24 2018

@author: cristopher
"""

import cv2
import numpy as np

img = cv2.imread('kidney.tif', cv2.IMREAD_GRAYSCALE)

# 150 < img < 240 = 255

thresh = np.logical_and((img > 150),(img <240))

gray_img = img[thresh]

cv2.namedWindow('img', cv2.WINDOW_KEEPRATIO)

cv2.imshow('img', img[thresh])

cv2.waitKey(0)

cv2.destroyAllWindows()

# 64 < img < 140 = 0